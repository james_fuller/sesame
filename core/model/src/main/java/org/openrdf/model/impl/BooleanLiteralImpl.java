package org.openrdf.model.impl;

/**
 * @deprecated since 4.0. Use {@link BooleanLiteral} instead.
 * @author Jeen Broekstra
 */
@Deprecated
public class BooleanLiteralImpl extends BooleanLiteral {

	/**
	 * @deprecated since 4.0. Use {@link BooleanLiteral#TRUE} instead.
	 */
	@Deprecated
	public static final BooleanLiteralImpl TRUE = new BooleanLiteralImpl(true);

	/**
	 * @deprecated since 4.0. Use {@link BooleanLiteral#FALSE} instead.
	 */
	@Deprecated
	public static final BooleanLiteralImpl FALSE = new BooleanLiteralImpl(false);

	@Deprecated
	protected BooleanLiteralImpl(boolean b) {
		super(b);
	}

}
